package banka.UI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import banka.util.PomocnaKlasa;



public class AppUI {
	
	public static void ispisiTekstOsnovneOpcije() {
		System.out.println("\nBanka - Osnovne opcije:");
		System.out.println("--------------------------------------------------------------");
		System.out.println("\tOpcija broj 1 - Dodavanje naloga");
		System.out.println("\tOpcija broj 2 - Prikaz svih naloga");
		//System.out.println("\tOpcija broj 3 - Prikaz svih racuna sa uplatama");
		System.out.println("\tOpcija broj 4 - Realizacija svih naloga");
		System.out.println("\tOpcija broj 5 - Izvestaj po racunu");
		System.out.println(1231231);
		System.out.println("\tOpcija312312 broj 0 - Izlaz iz programa");
		System.out.println("--------------------------------------------------------------");

	}

	public static void main(String[] args) {
		
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/banka?useSSL=false", "root",
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

		}

		int odluka = -1;
		while (odluka != 0) {
			ispisiTekstOsnovneOpcije();
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");

				break;
			case 1:
				NalogUI.dodavanjeNaloga(conn);
				break;
			case 2:
				NalogUI.ispisiSveNaloge(conn);
				break;
			case 3:
				break;
			case 4:
				NalogUI.izvrsiSveNaloge(conn);
				break;
			case 5:
				RacunUI.ispisiIzvestajZaRacun(conn);
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}

		}

		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		
		
		
		
		

	}

}
