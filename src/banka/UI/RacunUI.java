package banka.UI;

import java.sql.Connection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import banka.dao.NalogDAO;
import banka.dao.RacunDAO;
import banka.model.Nalog;
import banka.model.Racun;
import banka.util.PomocnaKlasa;

public class RacunUI {
	
	private static final Simp1232131leDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public static void ispisiIzvestajZaRacun(Connection conn) {
		
		System.out.println("Unesite sifru racuna:");
		String sifra = PomocnaKlasa.ocitajTekst();
		Racun racun = RacunDAO.getRacunBySifra(conn, sifra);
		if(racun == null) {
			System.out.println("Nepostojeci racun.");
			return;			
		}
		System.out.println("Unesite pocetni datum za pretragu:");
		Date pocetni = null;
		do {
		pocetni = PomocnaKlasa.ocitajDatum("dd.MM.yyyy.");
		if(pocetni == null)
			System.out.println("Pogresan unos");		
		}while (pocetni == null);
		Date krajnji = null;
		System.out.println("Unesite krajnji datum za pretragu:");
		do {
			krajnji = PomocnaKlasa.ocitajDatum("dd.MM.yyyy.");
			if(krajnji == null)
				System.out.println("Pogresan unos");		
			}while (krajnji == null || krajnji.compareTo(pocetni) <= 0);
		
		ArrayList<Nalog> rezultati = NalogDAO.getAllForRacun(conn, racun);
		
		for (int i = 0; i < rezultati.size(); i++) {
			if(rezultati.get(i).getDatumKreiranja().compareTo(krajnji) > 0 || rezultati.get(i).getDatumKreiranja().compareTo(pocetni) < 0)
				rezultati.remove(i);			
		}
		double sumaPrihoda = 0;
		double sumaRashoda = 0;
		int brojPrihoda = 0;
		int brojRashoda = 0;
		
		for (int i = 0; i < rezultati.size(); i++) {
			if(rezultati.get(i).getRacunPrimaoca().getId() == racun.getId()) {
				sumaPrihoda += rezultati.get(i).getIznos(); 
				brojPrihoda++;		
			}else {
				sumaRashoda += rezultati.get(i).getIznos(); 
				brojRashoda++;	
			}
		}
		System.out.println("========================================================================================================");	
		System.out.println("Za racun:");
		System.out.println(Racun.razunZaglavlje());
		System.out.println("--------------------------------------------------------------------------------------------------------");
		System.out.println(racun);
		System.out.println("\nU periodu od " + sdf.format(pocetni) + " do " + sdf.format(krajnji) + " postoje sledece transakcije: \n");
		System.out.println(Nalog.nalogZaglavlje());
		System.out.println("--------------------------------------------------------------------------------------------------------");
		for (int i = 0; i < rezultati.size(); i++)
			System.out.println(rezultati.get(i));
		System.out.println("--------------------------------------------------------------------------------------------------------\n");
		System.out.println("Ukupno uplata: " + brojPrihoda + " Ukupni prihod: " + sumaPrihoda);	
		System.out.println("Ukupno isplata: " + brojRashoda + " Ukupni rashod: " + sumaRashoda + "\n");		
		System.out.println("========================================================================================================");	

	}
		
		
}


