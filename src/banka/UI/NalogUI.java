package banka.UI;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;

import banka.dao.NalogDAO;
import banka.dao.RacunDAO;
import banka.model.Nalog;
import banka.model.Racun;
import banka.util.PomocnaKlasa;

public class NalogUI {

	
	
	/*dodavanje naloga (pri �emu se navode �ifre ra�una uplatioca i ra�una primaoca i iznos, 
	generi�u se trenutni datum i vreme, a po dodavanju nalog jo� uvek nije realizovan; raspolo�ivo 
	stanje oba ra�una se u trenutku kreiranja naloga umanjuje, odnosno uve�ava za navedeni iznos)
	NAPOMENA: pri formiranju naloga izvr�iti proveru raspolo�ivog stanja uplatioca naspram iznosa za uplatu
	NAPOMENA: obratiti pa�nju na transakcioni re�im zadavanja upita (koliko upita je potrebno izvr�iti?)*/
	
	
	public static void dodavanjeNaloga(Connection conn) {
		
		System.out.println("Unesite sifru uplatilaca");
		String uplatilac = PomocnaKlasa.ocitajTekst();
		Racun racunUplatioca = RacunDAO.getRacunBySifra(conn, uplatilac);
		
		System.out.println("Unesite sifru primaoca");
		String primalac = PomocnaKlasa.ocitajTekst();
		Racun racunPrimaoca = RacunDAO.getRacunBySifra(conn, primalac);
		
		System.out.println("Unesite iznos za uplatu");
		double iznos = PomocnaKlasa.ocitajRealanBroj();
		
		if(iznos > racunUplatioca.getRaspolozivoStanje()) {
			System.out.println("Nedovoljna sredstva");
			return;
		}
		
		double temp = racunUplatioca.getRaspolozivoStanje();
		racunUplatioca.setRaspolozivoStanje(temp - iznos);
		
		temp = racunPrimaoca.getRaspolozivoStanje();		
		racunPrimaoca.setRaspolozivoStanje(temp+iznos);
		
		Nalog nalog = new Nalog(0, racunUplatioca, racunPrimaoca, new Date(), iznos);
		NalogDAO.add(conn, nalog);
		
				
		
	}
	
	public static void izvrsiSveNaloge(Connection conn) {
		
		ArrayList<Nalog> neizvrseni = new ArrayList<>(NalogDAO.getAllNeizvrsene(conn));
		
		for (int i = 0; i < neizvrseni.size(); i++) {
			NalogDAO.izvrsiNalog(conn, neizvrseni.get(i));
		}
				
	}
	
	public static void ispisiSveNaloge(Connection conn) {
			
		ArrayList<Nalog> nalozi = new ArrayList<>(NalogDAO.getAll(conn));
		System.out.println(Nalog.nalogZaglavlje());
		for (int i = 0; i < nalozi.size(); i++) {
			System.out.println(nalozi.get(i));			
		}
		
		
		
		
		
	}
	
	
	
	
}
