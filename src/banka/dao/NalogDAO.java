package banka.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import banka.model.Nalog;
import banka.model.Racun;



public class NalogDAO {
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public static Nalog getNalogByID(Connection conn, int id) {
		Nalog nalog = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM nalozi WHERE id = " + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int idNaloga = rset.getInt(index++);
				int uplatilac = rset.getInt(index++);
				int primalac = rset.getInt(index++);
				Date kreiran = null;
				try {
					kreiran = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double iznos = rset.getDouble(index++);
				Date realizovan= null;
				try {
					if(rset.getString(index) == null)
						realizovan = null;
					else	
						realizovan = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				nalog = new Nalog(idNaloga, RacunDAO.getRacunById(conn, uplatilac), RacunDAO.getRacunById(conn, primalac), kreiran, iznos, realizovan);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return nalog;
	}
	
		
	public static List<Nalog> getAll(Connection conn) {
		ArrayList<Nalog> nalozi = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM nalozi";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int idNaloga = rset.getInt(index++);
				int uplatilac = rset.getInt(index++);
				int primalac = rset.getInt(index++);
				Date kreiran = null;
				try {
					kreiran = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double iznos = rset.getDouble(index++);
				Date realizovan = null;
				try {
					if(rset.getString(index) == null)
						realizovan = null;
					else	
						realizovan = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Nalog nalog =  new Nalog(idNaloga, RacunDAO.getRacunById(conn, uplatilac), RacunDAO.getRacunById(conn, primalac), kreiran, iznos, realizovan);
				nalozi.add(nalog);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nalozi;
	}
	
	public static List<Nalog> getAllNeizvrsene(Connection conn) {
		ArrayList<Nalog> nalozi = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM nalozi WHERE realizovan IS NULL";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int idNaloga = rset.getInt(index++);
				int uplatilac = rset.getInt(index++);
				int primalac = rset.getInt(index++);
				Date kreiran = null;
				try {
					kreiran = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double iznos = rset.getDouble(index++);
				Date realizovan = null;
				try {
					if(rset.getString(index) == null)
						realizovan = null;
					else	
						realizovan = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Nalog nalog =  new Nalog(idNaloga, RacunDAO.getRacunById(conn, uplatilac), RacunDAO.getRacunById(conn, primalac), kreiran, iznos, realizovan);
				nalozi.add(nalog);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nalozi;
	}

	public static boolean add(Connection conn, Nalog nalog) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO nalozi (uplatilac, primalac, kreiran, iznos) VALUES (?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, nalog.getRacunUplatioca().getId());
			pstmt.setInt(index++, nalog.getRacunPrimaoca().getId());
			pstmt.setString(index++, sdf.format(nalog.getDatumKreiranja()));
			pstmt.setDouble(index++, nalog.getIznos());
						
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
		
	public static void izvrsiNalog(Connection conn, Nalog nalog) {
		
		PreparedStatement pstmt = null;
		try {
			String query;
			int index;

			conn.setAutoCommit(false); // iskljucivanje automatske transakcije (pri cemu je svaki upit bio transakcija za sebe)
			conn.commit(); // pocetak transakcije
			
			// 1. SQL upit
			query = "UPDATE racuni SET stanje = stanje - ?, raspolozivoStanje = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			index = 1;
			pstmt.setDouble(index++, nalog.getIznos());
			pstmt.setDouble(index++, nalog.getRacunUplatioca().getRaspolozivoStanje());
			pstmt.setInt(index++, nalog.getRacunUplatioca().getId());
	
			pstmt.executeUpdate();
			pstmt.close();

			query = "UPDATE racuni SET stanje = stanje + ?, raspolozivoStanje = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			index = 1;
			pstmt.setDouble(index++, nalog.getIznos());
			pstmt.setDouble(index++, nalog.getRacunPrimaoca().getRaspolozivoStanje());
			pstmt.setInt(index++, nalog.getRacunPrimaoca().getId());
			
			pstmt.executeUpdate();
			pstmt.close();

			query = "UPDATE nalozi SET realizovan = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			index = 1;
			pstmt.setString(index++, sdf.format(new Date()));
			pstmt.setInt(index++, nalog.getId());
							
			pstmt.executeUpdate();
			
			conn.commit(); // kraj transakcije
		} catch (Exception ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
			try {conn.rollback();} catch (SQLException ex1) { ex1.printStackTrace(); } // vratiti bazu u stanje pre pocetka transakcije
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {conn.setAutoCommit(true);} catch (SQLException ex1) {ex1.printStackTrace();} // ukljuciti automatsku tranaskciju
		}
	}
	
	public static ArrayList<Nalog> getAllForRacun(Connection conn, Racun racun) {
		ArrayList<Nalog> nalozi = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM nalozi WHERE uplatilac = " + racun.getId() + " OR primalac = " + racun.getId();

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int idNaloga = rset.getInt(index++);
				int uplatilac = rset.getInt(index++);
				int primalac = rset.getInt(index++);
				Date kreiran = null;
				try {
					kreiran = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				double iznos = rset.getDouble(index++);
				Date realizovan = null;
				try {
					if(rset.getString(index) == null)
						realizovan = null;
					else	
						realizovan = sdf.parse(rset.getString(index++));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Nalog nalog =  new Nalog(idNaloga, RacunDAO.getRacunById(conn, uplatilac), RacunDAO.getRacunById(conn, primalac), kreiran, iznos, realizovan);
				nalozi.add(nalog);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nalozi;
	}
	

}
