package banka.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import banka.model.Nalog;
import banka.model.Racun;

public class RacunDAO {

	public static Racun getRacunById(Connection conn, int id) {
		Racun racun = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM racuni WHERE id = " + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int idRacuna = rset.getInt(index++);
				String sifra = rset.getString(index++);
				String vlasnik = rset.getString(index++);
				double stanje = rset.getDouble(index++);
				double raspolozivoStanje = rset.getDouble(index++);
				
				
				
				racun = new Racun(idRacuna, sifra, vlasnik, stanje, raspolozivoStanje);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return racun;
		
	}
	
	public static Racun getRacunBySifra(Connection conn, String sifra) {
		Racun racun = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM racuni WHERE sifra = ?";

			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, sifra);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				int index = 1;
				int idRacuna = rset.getInt(index++);
				String sifraRacuna = rset.getString(index++);
				String vlasnik = rset.getString(index++);
				double stanje = rset.getDouble(index++);
				double raspolozivoStanje = rset.getDouble(index++);
				
				
				
				racun = new Racun(idRacuna, sifraRacuna, vlasnik, stanje, raspolozivoStanje);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return racun;
		
	}

	public static List<Racun> getAll(Connection conn) {
		List<Racun> racuni = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM racuni";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int idRacuna = rset.getInt(index++);
				String sifra = rset.getString(index++);
				String vlasnik = rset.getString(index++);
				double stanje = rset.getDouble(index++);
				double raspolozivoStanje = rset.getDouble(index++);
				
				
				
				Racun racun = new Racun(idRacuna, sifra, vlasnik, stanje, raspolozivoStanje);
				racuni.add(racun);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return racuni;
	}

	
}
