package banka.model;

public class Racun {
	
	private int id;
	private String sifra;
	private String vlasnik;
	private double stanje;
	private double raspolozivoStanje;
	
	public Racun() {
		
	}

	public Racun(int id, String sifra, String vlasnik) {
		super();
		this.id = id;
		this.sifra = sifra;
		this.vlasnik = vlasnik;
	}

	public Racun(int id, String sifra, String vlasnik, double stanje, double raspolozivoStanje) {
		super();
		this.id = id;
		this.sifra = sifra;
		this.vlasnik = vlasnik;
		this.stanje = stanje;
		this.raspolozivoStanje = raspolozivoStanje;
	}

	public int getId() {
		return id;
	}

	public String getSifra() {
		return sifra;
	}

	public void setSifra(String sifra) {
		this.sifra = sifra;
	}

	public String getVlasnik() {
		return vlasnik;
	}

	public void setVlasnik(String vlasnik) {
		this.vlasnik = vlasnik;
	}

	public double getStanje() {
		return stanje;
	}

	public void setStanje(double stanje) {
		this.stanje = stanje;
	}

	public double getRaspolozivoStanje() {
		return raspolozivoStanje;
	}

	public void setRaspolozivoStanje(double raspolozivoStanje) {
		this.raspolozivoStanje = raspolozivoStanje;
	}

	public static String razunZaglavlje() {
		
		return String.format("| %10s | %30s | %10s | %18s |", "Sifra", "Vlasnik", "Stanje", "Raspolozivo Stanje");
		
	}
	
	@Override
	public String toString() {
		return  String.format("| %10s | %30s | %10s | %18s |", sifra, vlasnik, stanje, raspolozivoStanje);
				
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Racun other = (Racun) obj;
		if (id != other.id)
			return false;
		if (sifra == null) {
			if (other.sifra != null)
				return false;
		} else if (!sifra.equals(other.sifra))
			return false;
		return true;
	}
	
	
	
	
	
}
