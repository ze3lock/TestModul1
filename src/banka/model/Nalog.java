package banka.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Nalog {
	
	private int id;
	private Racun racunUplatioca;
	private Racun racunPrimaoca;
	private Date datumKreiranja;
	private double iznos;
	private Date datumRealizacije;
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public Nalog() {
		super();
	}

	public Nalog(int id, Racun racunUplatioca, Racun racunPrimaoca, Date datumKreiranja, double iznos) {
		this.id = id;
		this.racunUplatioca = racunUplatioca;
		this.racunPrimaoca = racunPrimaoca;
		this.datumKreiranja = datumKreiranja;
		this.iznos = iznos;
	}

	public Nalog(int id, Racun racunUplatioca, Racun racunPrimaoca, Date datumKreiranja, double iznos,
			Date datumRealizacije) {
		this.id = id;
		this.racunUplatioca = racunUplatioca;
		this.racunPrimaoca = racunPrimaoca;
		this.datumKreiranja = datumKreiranja;
		this.iznos = iznos;
		this.datumRealizacije = datumRealizacije;
	}

	public Racun getRacunUplatioca() {
		return racunUplatioca;
	}

	public void setRacunUplatioca(Racun racunUplatioca) {
		this.racunUplatioca = racunUplatioca;
	}

	public Racun getRacunPrimaoca() {
		return racunPrimaoca;
	}

	public void setRacunPrimaoca(Racun racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}

	public Date getDatumKreiranja() {
		return datumKreiranja;
	}

	public void setDatumKreiranja(Date datumKreiranja) {
		this.datumKreiranja = datumKreiranja;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}

	public Date getDatumRealizacije() {
		return datumRealizacije;
	}

	public void setDatumRealizacije(Date datumRealizacije) {
		this.datumRealizacije = datumRealizacije;
	}

	public int getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nalog other = (Nalog) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	public static String nalogZaglavlje() {
		
		return String.format("| %5s | %15s | %15s | %20s | %10s | %20s |", "Id", "Racun uplatioca", "Racun primaoca", "Datum Kreiranja", "Iznos", "Datum realizacije");
	}
	
	@Override
	public String toString() {
		return String.format("| %5s | %15s | %15s | %20s | %10s | %20s |", id, racunUplatioca.getSifra(), racunPrimaoca.getSifra(), sdf.format(datumKreiranja), iznos, datumRealizacije == null ? "Nije realizovan" : sdf.format(datumRealizacije));
	}

	
	
	
	
	
}
