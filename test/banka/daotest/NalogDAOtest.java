package banka.daotest;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import banka.dao.NalogDAO;
import banka.model.Nalog;
import banka.model.Racun;

public class NalogDAOtest {

Connection conn = null;
	
	@Before
	public void setUp() throws Exception {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/banka?useSSL=false", "root", "root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

		}

	}

	@After
	public void tearDown() throws Exception {
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}

	@Test
	public void test() {
		
		Racun ra1 = new Racun(10, "123", "abc", 250, 250);
		Racun ra2 = new Racun(11, "234", "cde", 250, 250);
		
		Nalog na1 = new Nalog(0, ra1, ra2, new Date(), 250.00);
		Nalog na2 = new Nalog(0, ra2, ra1, new Date(), 250.00);
		
		NalogDAO.add(conn, na1);
		NalogDAO.add(conn, na2);
		
		ArrayList<Nalog> lista =  NalogDAO.getAllForRacun(conn, ra1);
		assertTrue(lista.size() != 0);
		
		lista =  NalogDAO.getAllForRacun(conn, ra2);
		assertTrue(lista.size() != 0);
		
		
			
		
		
		
	}

}
