DROP SCHEMA IF EXISTS banka;
CREATE SCHEMA banka DEFAULT CHARACTER SET utf8;
USE banka;

CREATE TABLE racuni (
	id INT AUTO_INCREMENT, 
	sifra VARCHAR(10) NOT NULL,
	vlasnik VARCHAR(40) NOT NULL,
	stanje DOUBLE NOT NULL,
	raspolozivoStanje DOUBLE,
	
	PRIMARY KEY(id, sifra)
);
	
CREATE TABLE nalozi (
	id INT AUTO_INCREMENT,
	uplatilac INT NOT NULL,
	primalac INT NOT NULL,
	kreiran DATETIME NOT NULL,
	iznos DOUBLE NOT NULL,
	realizovan DATETIME,
	PRIMARY KEY(id),
	
	FOREIGN KEY (uplatilac) REFERENCES racuni(id)
		ON DELETE RESTRICT,
	FOREIGN KEY (primalac) REFERENCES racuni(id)
		ON DELETE RESTRICT
);
	


INSERT INTO racuni (id, sifra, vlasnik, stanje, raspolozivoStanje) VALUES (1 , '1a', 'a', 200.00, 0.00);
INSERT INTO racuni (id, sifra, vlasnik, stanje, raspolozivoStanje) VALUES (2 , '2b', 'b', 300.00, 0.00);
INSERT INTO racuni (id, sifra, vlasnik, stanje, raspolozivoStanje) VALUES (3 , '3c', 'c', 0.00, 500.00);
INSERT INTO racuni (id, sifra, vlasnik, stanje, raspolozivoStanje) VALUES (4 , '4d', 'd', 150.00, 150.00);
INSERT INTO racuni (id, sifra, vlasnik, stanje, raspolozivoStanje) VALUES (5 , '5e', 'e', 0.00, 0.00);




INSERT INTO nalozi (id, uplatilac, primalac, kreiran, iznos, realizovan) VALUES (1, 1 , 2, '2017-09-01 01:00:00', 100.00, '2017-09-01 23:59:00');
INSERT INTO nalozi (id, uplatilac, primalac, kreiran, iznos, realizovan) VALUES (2, 1 , 3, '2017-09-02 02:00:00', 200.00, NULL);
INSERT INTO nalozi (id, uplatilac, primalac, kreiran, iznos, realizovan) VALUES (3, 2 , 3, '2017-09-03 03:00:00', 300.00, NULL);